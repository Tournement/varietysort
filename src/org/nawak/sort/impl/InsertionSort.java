package org.nawak.sort.impl;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import org.nawak.sort.SortType;
class InsertionSort<E> extends AbstractSort<E> {
    public InsertionSort(Comparator<E> comparator) {
        super(comparator);
    }

    public InsertionSort() {
        this(null);
    }

    @Override
    protected List<E> doSort(List<E> input, Comparator<E> comparator) {
        LinkedList<E> ouput = new LinkedList<>();
        input.forEach(e -> insert(e, ouput, comparator));
        return ouput;
    }

    private void insert(E e, LinkedList<E> ouput, Comparator<E> comparator) {
        int i = 0;
        for (E elem : ouput) {
            if (comparator.compare(e, elem) < 0) {
                break;
            }
            i++;
        }
        ouput.add(i, e);
    }

    @Override
    public SortType getSortType() {
        return SortType.INSERTIONSORT;
    }
}
