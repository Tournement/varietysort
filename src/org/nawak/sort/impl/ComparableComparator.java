package org.nawak.sort.impl;

import java.util.Comparator;

final class ComparableComparator<T> implements Comparator<T> {
    @SuppressWarnings("unchecked")
    @Override
    public int compare(T o1, T o2) {
        return ((Comparable<T>) o1).compareTo(o2);
    }
}