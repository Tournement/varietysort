package org.nawak.sort.impl;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import org.nawak.sort.SortType;
class QuickSort<E> extends AbstractSort<E> {
    private final static class Pair {
        private int x;
        private int y;

        Pair(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }
    }

    public QuickSort(Comparator<E> comparator) {
        super(comparator);
    }

    public QuickSort() {
        super(null);
    }

    @Override
    protected List<E> doSort(List<E> input, Comparator<E> comparator) {
        @SuppressWarnings("unchecked")
        E[] elements = (E[]) input.toArray();
        quicksort(elements, 0, elements.length - 1, comparator);
        return Arrays.asList(elements);
    }

    @Override
    public SortType getSortType() {
        return SortType.QUICKSORT;
    }

    public void swap(E[] arr, int i, int j) {
        E temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    // Partition routine using Dutch National Flag Algorithm
    public Pair Partition(E[] arr, int start, int end, Comparator<E> comp) {
        int mid = start;
        E pivot = arr[end];
        while (mid <= end) {
            int compare = comp.compare(arr[mid], pivot);
            if (compare < 0) {
                swap(arr, start, mid);
                ++start;
                ++mid;
            } else if (compare > 0) {
                swap(arr, mid, end);
                --end;
            } else {
                ++mid;
            }
        }
        // arr[start .. mid - 1] contains all occurrences of pivot
        return new Pair(start - 1, mid);
    }

    public void quicksort(E[] arr, int start, int end, Comparator<E> comparator) {
        // base condition for 0 or 1 elements
        if (start >= end) {
            return;
        }
        // handle 2 elements separately as Dutch national flag
        // algorithm will work for 3 or more elements
        if (start - end == 1) {
            if (comparator.compare(arr[start], arr[end]) < 0) {
                swap(arr, start, end);
            }
            return;
        }
        // rearrange the elements across pivot using Dutch
        // national flag problem algorithm
        Pair pivot = Partition(arr, start, end, comparator);
        // recurse on sub-array containing elements that are less than pivot
        quicksort(arr, start, pivot.getX(), comparator);
        // recurse on sub-array containing elements that are more than pivot
        quicksort(arr, pivot.getY(), end, comparator);
    }
}
