package org.nawak.sort.impl;
import java.util.List;
@FunctionalInterface
public interface ISorter<E> {
    List<E> sort(List<E> input);
}
