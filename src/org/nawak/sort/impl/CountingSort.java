package org.nawak.sort.impl;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.nawak.sort.SortType;
import org.nawak.sort.story.StoryTeller;
class CountingSort<E> extends AbstractSort<E> {
    public CountingSort() {
        this(null);
    }

    public CountingSort(Comparator<E> comparator) {
        super(comparator);
    }

    @Override
    public List<E> doSort(List<E> input, Comparator<E> comparator) {
        getStoryTeller().tell("%@n start by grouping these %@0 ", StoryTeller.Items.Item.handle(input));
        HashMap<E, Integer> map = new HashMap<>();
        input.forEach(e -> fillMap(e, map));
        LinkedList<E> list = new LinkedList<>(map.keySet());
        getStoryTeller().tell("These group are : %@0 ", StoryTeller.Items.Collection.handle(list));
        Collection<E> sorted = new QuickSort<E>(comparator).sort(list);
        getStoryTeller().tell("Then %@gp sorts these group.");
        LinkedList<E> output = new LinkedList<>();
        sorted.forEach(e -> fillFinalList(output, map, e));
        getStoryTeller().tell("then put the group together");
        return output;
    }

    private void fillFinalList(LinkedList<E> output, HashMap<E, Integer> map, E e) {
        for (int i = 0; i < map.get(e); i++) {
            output.add(e);
        }
    }

    private void fillMap(E e, HashMap<E, Integer> map) {
        Integer lastcount = map.get(e);
        if (lastcount == null) {
            map.put(e, 1);
        } else {
            map.put(e, lastcount + 1);
        }
    }

    @Override
    public SortType getSortType() {
        return SortType.COUNTINGSORT;
    }
}
