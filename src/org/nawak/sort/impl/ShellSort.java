package org.nawak.sort.impl;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import org.nawak.sort.SortType;
import org.nawak.sort.story.StoryTeller;
class ShellSort<E> extends AbstractSort<E> {
    public ShellSort(Comparator<E> comparator) {
        super(comparator);
    }

    public ShellSort() {
        this(null);
    }

    @Override
    public SortType getSortType() {
        return SortType.SHELLSORT;
    }

    /**
     * this fonction is an apdation from
     * https://stackoverflow.com/a/4835281/9630748
     */
    @Override
    protected List<E> doSort(List<E> input, Comparator<E> comparator) {
        @SuppressWarnings("unchecked")
        E[] element = (E[]) input.toArray();
        getStoryTeller().tell("%@n decide to sort these elements using Shell's method : %@0", StoryTeller.Items.Collection.handle(input));
        int j;
        for (int gap = element.length / 2; gap > 0; gap /= 2) {
            for (int i = gap; i < element.length; i++) {
                E tmp = element[i];
                for (j = i; j >= gap && comparator.compare(tmp, element[j - gap]) < 0; j -= gap) {
                    element[j] = element[j - gap];
                }
                element[j] = tmp;
            }
        }
        return Arrays.asList(element);
    }
}
