package org.nawak.sort.impl;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import org.nawak.sort.SortType;
import org.nawak.sort.story.StoryTeller;
class BubbleSort<E> extends AbstractSort<E> {
    public BubbleSort() {
        this(null);
    }

    public BubbleSort(Comparator<E> comparator) {
        super(comparator);
    }

    @Override
    protected List<E> doSort(List<E> input, Comparator<E> comparator) {
        getStoryTeller().tell("%@n will sort this part now : %@0", StoryTeller.Items.Collection.handle(input));
        @SuppressWarnings("unchecked")
        E[] elements = (E[]) input.toArray();
        while (passAndSwap(comparator, elements, 1, false))
            ;
        return Arrays.asList(elements);
    }

    protected boolean passAndSwap(Comparator<E> comparator, E[] elements, int gap, boolean permuteHasBeendone) {
        for (int i = 0; i < elements.length - gap; i++) {
            permuteHasBeendone |= !checkOrSwap(i, i + gap, elements, comparator);
        }
        return permuteHasBeendone;
    }

    private boolean checkOrSwap(int i, int j, E[] elements, Comparator<E> comparator2) {
        if (comparator2.compare(elements[i], elements[j]) > 0) {
            E tmp = elements[i];
            getStoryTeller().tell("%@n swap the elements at " + i + " and " + j);
            elements[i] = elements[j];
            elements[j] = tmp;
            return false;
        } else {
            return true;
        }
    }

    @Override
    public SortType getSortType() {
        return SortType.BUBBLESORT;
    }
}
