package org.nawak.sort.impl;
import java.util.Collection;

import org.nawak.sort.SortType;
import org.nawak.sort.story.StoryTeller;
public interface ISort<E> extends ISorter<E> {
    StoryTeller instance = new StoryTeller();

    SortType getSortType();

    default boolean isCollectionOfComparable(Collection<E> bazard) {
        return true;
    }

    default StoryTeller getStoryTeller() {
        return instance;
    }
}
