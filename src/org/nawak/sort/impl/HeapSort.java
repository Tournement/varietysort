package org.nawak.sort.impl;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import org.nawak.sort.SortType;
class HeapSort<E> extends AbstractSort<E> {
    public HeapSort(Comparator<E> comparator) {
        super(comparator);
    }

    public HeapSort() {
        this(null);
    }

    @Override
    public SortType getSortType() {
        return SortType.HEAPSORT;
    }

    @Override
    public List<E> doSort(List<E> input, Comparator<E> comp) {
        @SuppressWarnings("unchecked")
        E[] elements = (E[]) input.toArray();
        int n = elements.length;
        // Build heap (rearrange array)
        for (int i = n / 2 - 1; i >= 0; i--)
            heapify(elements, n, i, comp);
        // One by one extract an element from heap
        for (int i = n - 1; i >= 0; i--) {
            // Move current root to end
            E temp = elements[0];
            elements[0] = elements[i];
            elements[i] = temp;
            // call max heapify on the reduced heap
            heapify(elements, i, 0, comp);
        }
        return Arrays.asList(elements);
    }

    // To heapify a subtree rooted with node i which is
    // an index in arr[]. n is size of heap
    void heapify(E arr[], int n, int i, Comparator<E> comp) {
        int largest = i; // Initialize largest as root
        int l = 2 * i + 1; // left = 2*i + 1
        int r = 2 * i + 2; // right = 2*i + 2
        // If left child is larger than root
        if (l < n && comp.compare(arr[l], arr[largest]) > 0)
            largest = l;
        // If right child is larger than largest so far
        if (r < n && comp.compare(arr[r], arr[largest]) > 0)
            largest = r;
        // If largest is not root
        if (largest != i) {
            E swap = arr[i];
            arr[i] = arr[largest];
            arr[largest] = swap;
            // Recursively heapify the affected sub-tree
            heapify(arr, n, largest, comp);
        }
    }
}
