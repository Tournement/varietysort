package org.nawak.sort.impl;
import java.util.Comparator;

import org.nawak.sort.SortType;
public class SortFactory {
    public static final SortFactory instance = new SortFactory();

    public <E> ISort<E> createRandomSort(Comparator<E> comparator) {
        return createSort(SortType.pickRandomSOrtImplem(), comparator);
    }

    public <E> ISort<E> createRandomSort() {
        return createSort(SortType.pickRandomSOrtImplem());
    }

    public <E> ISort<E> createSort(SortType pickRandomSOrtImplem) {
        return createSort(pickRandomSOrtImplem, null);
    }

    public <E> ISort<E> createSort(SortType pickRandomSOrtImplem, Comparator<E> comparator) {
        switch (pickRandomSOrtImplem) {
            case BUBBLESORT :
                return createBubbleSort(comparator);
            case COMBSORT :
                return createCombSort(comparator);
            case COUNTINGSORT :
                return createCountingSort(comparator);
            case HEAPSORT :
                return createHeapSort(comparator);
            case INSERTIONSORT :
                return createInsertionSort(comparator);
            case MERGESORT :
                return createMergeSort(comparator);
            case QUICKSORT :
                return createQuickSort(comparator);
            case SHUFFLESORT :
                return createShuffleSort(comparator);
            case SELECTIONSORT :
                return createSelectionSort(comparator);
            case SHELLSORT :
                return createShellSort(comparator);
            case VARIETYSORT :
                return createVarietySort(comparator);
            default :
                throw new IllegalArgumentException();
        }
    }

    public <E> ISort<E> createVarietySort(Comparator<E> comparator) {
        return new VarietySort<E>(comparator);
    }

    public <E> ISort<E> createCountingSort(Comparator<E> comparator) {
        return new CountingSort<E>(comparator);
    }

    public <E> ISort<E> createBubbleSort(Comparator<E> comparator) {
        return new BubbleSort<E>(comparator);
    }

    public <E> ISort<E> createCombSort(Comparator<E> comparator) {
        return new CombSort<E>(comparator);
    }

    public <E> ISort<E> createHeapSort(Comparator<E> comparator) {
        return new HeapSort<E>(comparator);
    }

    public <E> ISort<E> createInsertionSort(Comparator<E> comparator) {
        return new InsertionSort<E>(comparator);
    }

    public <E> ISort<E> createMergeSort(Comparator<E> comparator) {
        return new MergeSort<E>(comparator);
    }

    public <E> ISort<E> createQuickSort(Comparator<E> comparator) {
        return new QuickSort<E>(comparator);
    }

    public <E> ISort<E> createShuffleSort(Comparator<E> comparator) {
        return new ShuffleSort<E>(comparator);
    }

    public <E> ISort<E> createSelectionSort(Comparator<E> comparator) {
        return new SelectionSort<E>(comparator);
    }

    public <E> ISort<E> createShellSort(Comparator<E> comparator) {
        return new ShellSort<E>(comparator);
    }

    public <E> ISort<E> createVarietySort() {
        return new VarietySort<E>(null);
    }

    public <E> ISort<E> createCountingSort() {
        return new CountingSort<E>(null);
    }

    public <E> ISort<E> createBubbleSort() {
        return new BubbleSort<E>(null);
    }

    public <E> ISort<E> createCombSort() {
        return new CombSort<E>(null);
    }

    public <E> ISort<E> createHeapSort() {
        return new HeapSort<E>(null);
    }

    public <E> ISort<E> createInsertionSort() {
        return new InsertionSort<E>(null);
    }

    public <E> ISort<E> createMergeSort() {
        return new MergeSort<E>(null);
    }

    public <E> ISort<E> createQuickSort() {
        return new QuickSort<E>(null);
    }

    public <E> ISort<E> createShuffleSort() {
        return new ShuffleSort<E>(null);
    }

    public <E> ISort<E> createSelectionSort() {
        return new SelectionSort<E>(null);
    }

    public <E> ISort<E> createShellSort() {
        return new ShellSort<E>(null);
    }
}
