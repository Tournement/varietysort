package org.nawak.sort.impl;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import org.nawak.sort.SortType;
class MergeSort<E> extends AbstractSort<E> {
    public MergeSort(Comparator<E> comparator) {
        super(comparator);
    }

    public MergeSort() {
        this(null);
    }

    @Override
    public SortType getSortType() {
        return SortType.MERGESORT;
    }

    @Override
    protected List<E> doSort(List<E> input, Comparator<E> comparableComparator) {
        @SuppressWarnings("unchecked")
        E[] gArray = (E[]) input.toArray();
        E[] fArray = Arrays.copyOf(gArray, gArray.length);
        mergeSort3WayRec(fArray, 0, gArray.length, gArray, comparableComparator);
        return Arrays.asList(fArray);
    }

    /*
     * Performing the merge sort algorithm on the
     * given array of values in the rangeof indices
     * [low, high). low is minimum index, high is
     * maximum index (exclusive)
     */
    public void mergeSort3WayRec(E[] gArray, int low, int high, E[] destArray, Comparator<E> comp) {
        // If array size is 1 then do nothing
        if (high - low < 2)
            return;
        // Splitting array into 3 parts
        int mid1 = low + ((high - low) / 3);
        int mid2 = low + 2 * ((high - low) / 3) + 1;
        // Sorting 3 arrays recursively
        mergeSort3WayRec(destArray, low, mid1, gArray, comp);
        mergeSort3WayRec(destArray, mid1, mid2, gArray, comp);
        mergeSort3WayRec(destArray, mid2, high, gArray, comp);
        // Merging the sorted arrays
        merge(destArray, low, mid1, mid2, high, gArray, comp);
    }

    /*
     * Merge the sorted ranges [low, mid1), [mid1,
     * mid2) and [mid2, high) mid1 is first midpoint
     * index in overall range to merge mid2 is second
     * midpoint index in overall range to merge
     */
    public void merge(E[] gArray, int low, int mid1, int mid2, int high, E[] destArray, Comparator<E> comp) {
        int i = low, j = mid1, k = mid2, l = low;
        // choose smaller of the smallest in the three ranges
        while (i < mid1 && j < mid2 && k < high) {
            if (comp.compare(gArray[i], gArray[j]) < 0) {
                if (comp.compare(gArray[i], gArray[k]) < 0)
                    destArray[l++] = gArray[i++];
                else
                    destArray[l++] = gArray[k++];
            } else {
                if (comp.compare(gArray[j], gArray[k]) < 0)
                    destArray[l++] = gArray[j++];
                else
                    destArray[l++] = gArray[k++];
            }
        }
        // case where first and second ranges have
        // remaining values
        while (i < mid1 && j < mid2) {
            if (comp.compare(gArray[i], gArray[j]) < 0)
                destArray[l++] = gArray[i++];
            else
                destArray[l++] = gArray[j++];
        }
        // case where second and third ranges have
        // remaining values
        while (j < mid2 && k < high) {
            if (comp.compare(gArray[j], gArray[k]) < 0)
                destArray[l++] = gArray[j++];
            else
                destArray[l++] = gArray[k++];
        }
        // case where first and third ranges have
        // remaining values
        while (i < mid1 && k < high) {
            if (comp.compare(gArray[i], gArray[k]) < 0)
                destArray[l++] = gArray[i++];
            else
                destArray[l++] = gArray[k++];
        }
        // copy remaining values from the first range
        while (i < mid1)
            destArray[l++] = gArray[i++];
        // copy remaining values from the second range
        while (j < mid2)
            destArray[l++] = gArray[j++];
        // copy remaining values from the third range
        while (k < high)
            destArray[l++] = gArray[k++];
    }
}
