package org.nawak.sort.impl;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import org.nawak.sort.SortType;
import org.nawak.sort.story.StoryTeller;
class CombSort<E> extends BubbleSort<E> {
    public CombSort(Comparator<E> comparator) {
        super(comparator);
    }

    public CombSort() {
        this(null);
    }

    @Override
    public SortType getSortType() {
        return SortType.COMBSORT;
    }

    @Override
    protected List<E> doSort(List<E> input, Comparator<E> comparator) {
        @SuppressWarnings("unchecked")
        E[] elements = (E[]) input.toArray();
        getStoryTeller().tell("%@n will sort this part now : %@0", StoryTeller.Items.Collection.handle(input));
        double shrink = 1.3; // Set the gap shrink factor
        int gap = Math.max(1, (int) Math.floor(input.size() / shrink));
        while (passAndSwap(comparator, elements, gap, gap == 1)) {
            gap = Math.min(1, (int) Math.floor(gap / shrink));
        }
        while (passAndSwap(comparator, elements, 1, false))
            ;
        return Arrays.asList(elements);
    }
}
