package org.nawak.sort.impl;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

import org.nawak.sort.DataInput;
import org.nawak.sort.SortType;
public class VarietySort<E> extends AbstractSort<E> implements ISort<E> {
    public VarietySort(Comparator<E> comp) {
        super(comp);
    }

    public VarietySort() {
        this(null);
    }

    @Override
    public SortType getSortType() {
        return SortType.VARIETYSORT;
    }

    @Override
    protected List<E> doSort(List<E> input, Comparator<E> comparableComparator) {
        DataInput<E> data = new DataInput<>(input);
        DataInput<E> output = new DataInput<>(new ArrayList<E>(input.size()));
        Collection<DataInput<E>> inputs = data.split(input.size() / 15);
        inputs.forEach(e -> e.applySort(SortFactory.instance.createRandomSort(comparableComparator)));
        inputs.forEach(e -> output.merge(e, comparableComparator));
        return output.getList();
    }
}
