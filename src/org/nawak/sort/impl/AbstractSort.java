package org.nawak.sort.impl;
import java.util.Comparator;
import java.util.List;
abstract class AbstractSort<E> implements ISort<E> {
    private Comparator<E> comparator;

    public AbstractSort() {
        this(null);
    }

    public AbstractSort(Comparator<E> comparator) {
        this.comparator = comparator;
    }

    @Override
    public final List<E> sort(List<E> input) {
        if (input.isEmpty()) {
            return input;
        }
        E firstElem = input.iterator().next();
        if (comparator != null) {
            return doSort(input, comparator);
        } else if (firstElem instanceof Comparable) {
            return doSort(input, new ComparableComparator<E>());
        }
        throw new IllegalArgumentException("can 't compare uncomparable elements without comparator");
    }

    protected abstract List<E> doSort(List<E> input, Comparator<E> comparableComparator);
}
