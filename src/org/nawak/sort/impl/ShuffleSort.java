package org.nawak.sort.impl;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.nawak.sort.SortType;
class ShuffleSort<E> extends AbstractSort<E> {
    public ShuffleSort(Comparator<E> comparator) {
        super(comparator);
    }

    public ShuffleSort() {
        this(null);
    }

    @Override
    public SortType getSortType() {
        return SortType.SHUFFLESORT;
    }

    @Override
    protected List<E> doSort(List<E> input, Comparator<E> comparator) {
        ArrayList<E> output = new ArrayList<>(input);
        while (!checkSort(output, comparator)) {
            Collections.shuffle(output);
        }
        return output;
    }

    private boolean checkSort(List<E> input, Comparator<E> comparator) {
        E previous = null;
        for (E elem : input) {
            if (previous != null && comparator.compare(previous, elem) > 0) {
                return false;
            }
            previous = elem;
        }
        return true;
    }
}
