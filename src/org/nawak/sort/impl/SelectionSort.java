package org.nawak.sort.impl;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import org.nawak.sort.SortType;
class SelectionSort<E> extends AbstractSort<E> {
    public SelectionSort(Comparator<E> comparator) {
        super(comparator);
    }

    public SelectionSort() {
        this(null);
    }

    @Override
    public SortType getSortType() {
        return SortType.SELECTIONSORT;
    }

    @Override
    protected List<E> doSort(List<E> input, Comparator<E> comparator) {
        @SuppressWarnings("unchecked")
        E[] elem = (E[]) input.toArray();
        for (int i = 0; i < elem.length; i++) {
            int minIndex = i;
            for (int j = i + 1; j < elem.length; j++) {
                if (comparator.compare(elem[minIndex], elem[j]) > 0) {
                    minIndex = j;
                }
            }
            if (minIndex != i) {
                E tmp = elem[i];
                elem[i] = elem[minIndex];
                elem[minIndex] = tmp;
            }
        }
        return Arrays.asList(elem);
    }
}
