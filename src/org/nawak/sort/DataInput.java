package org.nawak.sort;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import org.nawak.sort.impl.ISort;
public class DataInput<E> implements Iterable<E> {
    List<E> data;

    public DataInput(List<E> data) {
        this.data = new ArrayList<>(data);
    }

    @Override
    public ListIterator<E> iterator() {
        return data.listIterator();
    }

    public Collection<DataInput<E>> split(int nbOfSubList) {
        if (nbOfSubList < 2) {
            throw new IllegalArgumentException("DataInput can't be split in less that 2 DataInputs");
        } else {
            LinkedList<DataInput<E>> output = new LinkedList<DataInput<E>>();
            int normalSize = data.size() / nbOfSubList;
            int nbOfLongList = data.size() % nbOfSubList;
            int longSize = normalSize + 1;
            int lowerIndex = 0;
            int upperIndex = lowerIndex + normalSize;
            for (int i = 0; i < nbOfLongList; i++) {
                output.add(new DataInput<E>(data.subList(lowerIndex, upperIndex)));
                lowerIndex = upperIndex;
                upperIndex = lowerIndex + longSize;
            }
            for (int i = nbOfLongList; i < nbOfSubList; i++) {
                output.add(new DataInput<E>(data.subList(lowerIndex, upperIndex)));
                lowerIndex = upperIndex;
                upperIndex = lowerIndex + normalSize;
            }
            return output;
        }
    }

    public void applySort(ISort<E> sort) {
        data = sort.sort(data);
    }

    public void merge(DataInput<E> input, Comparator<E> comp) {
        Iterator<E> it = new SorterIterator<>(data.iterator(), input.data.iterator(), comp);
        ArrayList<E> target = new ArrayList<>(data.size() + input.data.size());
        while (it.hasNext()) {
            target.add(it.next());
        }
        data = target;
    }

    public List<E> getList() {
        return new ArrayList<>(data);
    }
}
