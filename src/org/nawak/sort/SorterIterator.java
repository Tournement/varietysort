package org.nawak.sort;
import java.util.Comparator;
import java.util.Iterator;
public class SorterIterator<E> implements Iterator<E> {
    private static final class IteratorWrapper<T> {
        private Iterator<T> iterator;
        private boolean haveNext;
        private T current;

        public IteratorWrapper(Iterator<T> iterator) {
            this.iterator = iterator;
            accept();
        }

        public T getCurrent() {
            return current;
        }

        public void accept() {
            haveNext = iterator.hasNext();
            if (haveNext) {
                current = iterator.next();
            }
        }

        public boolean hasNext() {
            return haveNext;
        }
    }

    private IteratorWrapper<E> iterator;
    private IteratorWrapper<E> iterator2;
    private Comparator<E> comp;

    public SorterIterator(Iterator<E> iterator, Iterator<E> iterator2, Comparator<E> comp) {
        this.iterator = new IteratorWrapper<>(iterator);
        this.iterator2 = new IteratorWrapper<>(iterator2);
        this.comp = comp;
    }

    @Override
    public boolean hasNext() {
        return iterator.hasNext() || iterator2.hasNext();
    }

    @Override
    public E next() {
        if (iterator.hasNext() ^ iterator2.hasNext()) {
            if (iterator.hasNext()) {
                return acceptAndReturn(iterator);
            } else {
                return acceptAndReturn(iterator2);
            }
        } else {
            if (comp.compare(iterator.getCurrent(), iterator2.getCurrent()) < 0) {
                return acceptAndReturn(iterator);
            } else {
                return acceptAndReturn(iterator2);
            }
        }
    }

    private E acceptAndReturn(IteratorWrapper<E> iterator) {
        E value = iterator.getCurrent();
        iterator.accept();
        return value;
    }
}
