package org.nawak.sort.story;
import java.util.Arrays;
import java.util.Collection;
public class StoryTeller implements IStoryTeller {
    public enum Items {
        Item,
        Collection;
        public <E> String handle(Collection<E> input) {
            switch (this) {
                case Collection :
                    return Arrays.deepToString(input.toArray());
                case Item :
                    return input.isEmpty() ? "Object" : input.iterator().next().getClass().getSimpleName();
            }
            return "";
        }
    }

    private IStoryTeller implem;

    public StoryTeller() {
        this(new StringBuilder());
    }

    public StoryTeller(StringBuilder buffer) {
        super();
        switchtoWriter();
    }

    public void switchtoWriter() {
        implem = new StoryTellerWritter("George", "he");
    }

    public void switchtoMute() {
        implem = new StoryTellerMute();
    }

    @Override
    public void tell(String string, String... handle) {
        implem.tell(string, handle);
    }

    @Override
    public void push(String string) {
        implem.push(string);
    }

    @Override
    public void readMyStory() {
        implem.readMyStory();
    }
}
