package org.nawak.sort.story;
public class StoryTellerMute implements IStoryTeller {
    @Override
    public void tell(String string, String... handle) {
        // do nothing
    }

    @Override
    public void push(String string) {
        // do nothing
    }

    @Override
    public void readMyStory() {
        // do nothing
    }
}
