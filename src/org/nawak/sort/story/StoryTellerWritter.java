package org.nawak.sort.story;
public class StoryTellerWritter implements IStoryTeller {
    private StringBuilder buffer;
    private String name;
    private String proname;

    public StoryTellerWritter(String name, String proname) {
        this.name = name;
        this.proname = proname;
        this.buffer = new StringBuilder();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.nawak.sort.story.IStoryTeller#tell(java.lang.String,
     * java.lang.String)
     */
    @Override
    public void tell(String string, String... handle) {
        int i = 0;
        string = string.replaceAll("%@n", name);
        string = string.replaceAll("%@gp", proname);
        for (String elem : handle) {
            string = string.replaceAll("%@" + i++, elem);
        }
        push(string);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.nawak.sort.story.IStoryTeller#push(java.lang.String)
     */
    @Override
    public void push(String string) {
        getBuffer().append(string + "\n");
    }

    private StringBuilder getBuffer() {
        return buffer;
    }

    @Override
    public void readMyStory() {
        System.out.println(buffer);
    }
}