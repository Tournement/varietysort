package org.nawak.sort.story;
public interface IStoryTeller {
    void tell(String string, String... handle);

    void push(String string);

    void readMyStory();
}