package org.nawak.sort;
import java.util.List;

import org.nawak.sort.impl.ISort;
import org.nawak.sort.impl.SortFactory;
public class Main {
    public static void main(String[] args) {
        List<Integer> list = DataInputTest.feed(10000);
        ISort<Integer> sort = SortFactory.instance.createVarietySort();
        sort.sort(list);
        sort.getStoryTeller().readMyStory();
    }
}
