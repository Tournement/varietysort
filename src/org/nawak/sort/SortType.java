package org.nawak.sort;
import java.util.ArrayList;
import java.util.Random;
public enum SortType {
    BUBBLESORT(1),
    COMBSORT(1),
    COUNTINGSORT(1),
    HEAPSORT(1),
    INSERTIONSORT(1),
    MERGESORT(1),
    QUICKSORT(1),
    SHUFFLESORT(0),
    VARIETYSORT(0),
    SELECTIONSORT(1),
    SHELLSORT(1);
    private static final SortType[] array = buildArray();
    private static final Random randomiser = new Random();

    private static SortType[] buildArray() {
        ArrayList<SortType> output = new ArrayList<SortType>();
        for (SortType implem : values()) {
            for (int i = 0; i < implem.getWeight(); i++) {
                output.add(implem);
            }
        }
        return output.toArray(new SortType[output.size()]);
    }

    public static SortType pickRandomSOrtImplem() {
        return array[randomiser.nextInt(array.length)];
    }

    private int weight;

    private SortType(int weight) {
        this.weight = weight;
    }

    double getPropability() {
        return ((double) getWeight()) / array.length;
    }

    int getWeight() {
        return weight;
    }
}
