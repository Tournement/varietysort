package org.nawak.sort;
import java.util.LinkedList;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
public class SortTypeTest {
    private static final class SortTypedTestRunner {
        private double variance;
        private TestUtils owntTestUtils;
        private TestUtils tranferedUtils;

        public SortTypedTestRunner(TestUtils owntTestUtils, TestUtils tranferedUtils) {
            this.owntTestUtils = owntTestUtils;
            this.tranferedUtils = tranferedUtils;
        }

        public SortTypedTestRunner run() {
            setVariance(test(Double.POSITIVE_INFINITY, NB_OF_TEST));
            return this;
        }

        public void log() {
            owntTestUtils.printProgress(1);
        }

        public double getVariance() {
            return variance;
        }

        public void setVariance(double variance) {
            this.variance = variance;
        }

        double test(double variancelimit, int nbOfTest) {
            SortType[] values = SortType.values();
            int[] count = new int[values.length];
            for (int i = 0; i < nbOfTest; i++) {
                SortType rand = SortType.pickRandomSOrtImplem();
                count[rand.ordinal()]++;
                tranferedUtils.printProgress(1);
            }
            printData(count, values, tranferedUtils);
            return assertDistribution(count, values, variancelimit, nbOfTest, tranferedUtils);
        }

        private double assertDistribution(int[] count, SortType[] values, double varianceLimit, int nbOfTest, TestUtils utils) {
            double variance = 0d;
            for (int i = 0; i < count.length; i++) {
                double diff = Double.valueOf(count[i]) / nbOfTest - values[i].getPropability();
                variance += diff * diff;
            }
            variance = Math.sqrt(variance);
            utils.println("Variance : " + variance);
            Assert.assertTrue(variance < varianceLimit);
            return variance;
        }

        private void printData(int[] count, SortType[] values, TestUtils utils) {
            utils.println("\n");
            for (int i = 0; i < count.length; i++) {
                utils.println(" - " + String.valueOf(values[i]) + " -> " + String.valueOf(count[i]));
            }
        }
    }

    private static final double VARIANCE_LIMIT = 1E-3;
    private static final int NB_OF_TEST = Integer.MAX_VALUE;

    @Ignore("too long for testsuit")
    @Test
    public void testVarianceConstance() {
        int nbofRun = 300;// Integer.MAX_VALUE;
        TestUtils owntTestUtils = new TestUtils(nbofRun);
        double maxVariance = Double.NEGATIVE_INFINITY;
        double minVariance = Double.POSITIVE_INFINITY;
        LinkedList<SortTypedTestRunner> batteryOfTest = new LinkedList<>();
        for (int i = 0; i < nbofRun; i++) {
            batteryOfTest.add(new SortTypedTestRunner(owntTestUtils, new TestUtils(false, true, NB_OF_TEST)));
        }
        batteryOfTest.forEach(e -> e.run().log());
        for (SortTypedTestRunner test : batteryOfTest) {
            double variance = test.getVariance();
            if (maxVariance < variance) {
                maxVariance = variance;
            }
            if (minVariance > variance) {
                minVariance = variance;
            }
        }
        int coef = (int) Math.log10(maxVariance);
        double interesstinVaraianceValue = Math.pow(10, coef);
        System.out.println(minVariance);
        System.out.println(maxVariance);
        String advice = "An interssing Value will be " + interesstinVaraianceValue + ".";
        Assert.assertTrue("your limit variance is to low for this size of data : you have few chance to see your test pass." + advice, minVariance < VARIANCE_LIMIT);
        Assert.assertTrue("your limit variance is a little to low for this size of data : your result will be random." + advice, maxVariance < VARIANCE_LIMIT);
    }

    @Test
    public void testRandomiser() {
        new SortTypedTestRunner(null, new TestUtils(NB_OF_TEST)).test(VARIANCE_LIMIT, NB_OF_TEST);
    }
}
