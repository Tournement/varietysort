package org.nawak.sort.impl;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.nawak.sort.DataInputTest;
import org.nawak.sort.SortType;
public class SortFactoryTest {
    @Test
    public void testCreateSort() {
        StringBuilder sb = new StringBuilder();
        for (SortType type : SortType.values()) {
            if (SortFactory.instance.createSort(type).getSortType() != type) {
                sb.append(type + "didn't construct the right kind of Sorter\n");
            }
        }
        String s = sb.toString();
        Assert.assertTrue(s, s.isEmpty());
    }

    @Test
    public void testSorter() {
        StringBuilder sb = new StringBuilder();
        List<Integer> list = DataInputTest.feed(100);
        ArrayList<Integer> reference = new ArrayList<>(list);
        Collections.sort(reference);
        for (SortType type : SortType.values()) {
            if (type == SortType.SHUFFLESORT) {
                testshuffle(sb);
                continue;
            }
            System.out.println(type);
            ISort<Integer> createSort = SortFactory.instance.<Integer>createSort(type);
            createSort.getStoryTeller().switchtoMute();
            Collections.shuffle(list);
            Collection<Integer> sortedList = createSort.sort(list);
            Assert.assertFalse(type + " is not fully implemented.", sortedList == null);
            Object[] output = sortedList.toArray();
            Object[] ref = reference.toArray();
            displayResult(output, ref);
            try {
                Assert.assertArrayEquals("The type " + type + " didn't sort correty", ref, output);
            } catch (AssertionError e) {
                sb.append(e.getMessage());
            }
        }
        String s = sb.toString();
        Assert.assertTrue(s, s.isEmpty());
    }

    private void displayResult(Object[] output, Object[] ref) {
        if (!isVerbose()) {
            return;
        }
        System.out.println("output");
        System.out.println(Arrays.deepToString(output));
        System.out.println("ref");
        System.out.println(Arrays.deepToString(ref));
    }

    private boolean isVerbose() {
        return false;
    }

    private void testshuffle(StringBuilder sb) {
        ISort<Integer> createSort = SortFactory.instance.<Integer>createSort(SortType.SHUFFLESORT);
        createSort.getStoryTeller().switchtoMute();
        LinkedList<Integer> input = new LinkedList<Integer>();
        input.add(2);
        input.add(3);
        input.add(1);
        Collection<Integer> sortedList = createSort.sort(input);
        Object[] expecteds = new Integer[]{1, 2, 3};
        Object[] array = sortedList.toArray();
        displayResult(array, expecteds);
        try {
            Assert.assertArrayEquals("The type " + SortType.SHUFFLESORT + " didn't sort correty", expecteds, array);
        } catch (AssertionError e) {
            sb.append(e.getMessage());
        }
    }
}
