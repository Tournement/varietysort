package org.nawak.sort;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import org.nawak.sort.impl.SortFactoryTest;
@RunWith(Suite.class)
@SuiteClasses({SortTypeTest.class, DataInputTest.class, SortFactoryTest.class})
public class AllTests {
}
