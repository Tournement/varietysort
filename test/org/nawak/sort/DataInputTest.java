/**
 * 
 */
package org.nawak.sort;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Random;

import org.junit.Assert;
import org.junit.Test;
/**
 * @author arnau
 *
 */
public class DataInputTest {
    @Test
    public void testSplitin2() {
        testonmultiple(2);
        testonprime(2);
    }

    @Test
    public void testSplitin3() {
        testonmultiple(3);
        testonprime(3);
    }

    @Test
    public void testSplitin5() {
        testonmultiple(5);
        testonprime(5);
    }

    @Test
    public void testSplitin7() {
        testonmultiple(7);
        testonprime(7);
    }

    @Test
    public void testSplitBenchMark() {
        testonmultiple(7, 100000);
        testonprime(7, 99998);
    }

    private void testonprime(int i) {
        testonprime(i, 163);
    }

    private void testonmultiple(int i) {
        testonmultiple(i, 30);
    }

    private void testonmultiple(int i, double amplitude) {
        Random rand = new Random();
        int size = ((int) (rand.nextDouble() * amplitude)) * i;
        List<?> data = feed(size);
        test(data, i);
    }

    private <E> void test(List<E> data, int i) {
        DataInput<E> dataInput = new DataInput<>(data);
        Collection<DataInput<E>> splited = dataInput.split(i);
        Assert.assertEquals("Splitted haven't the right Size", splited.size(), i);
        splited.forEach(e -> checkAndRemove(data, e));
        Assert.assertTrue("this is not a partition, missing element are : " + Arrays.toString(data.toArray()), data.isEmpty());
    }

    public static ArrayList<Integer> feed(int size) {
        ArrayList<Integer> output = new ArrayList<>(size);
        Random rand = new Random();
        for (int i = 0; i < size; i++) {
            output.add(rand.nextInt());
        }
        return output;
    }

    private void checkAndRemove(List<?> data, DataInput<?> input) {
        input.forEach(e -> removeOrThrow(data, e));
    }

    private void removeOrThrow(List<?> data, Object e) {
        Assert.assertTrue(data.remove(e));
    }

    private void testonprime(int i, double prime) {
        Random rand = new Random();
        int size = ((int) (rand.nextDouble() * prime));
        List<?> data = feed(size);
        test(data, i);
    }
}
