package org.nawak.sort;
import java.util.Collections;
import java.util.concurrent.TimeUnit;
public class TestUtils {
    private int percent;
    private boolean outputEnable;
    private boolean ouputMinimized;
    private int total;
    private long startTime;
    private int current;

    public TestUtils(int percent, boolean outputEnable, boolean ouputMinimized, long startTime, int total) {
        super();
        this.percent = percent;
        this.outputEnable = outputEnable;
        this.ouputMinimized = ouputMinimized;
        this.startTime = startTime;
        this.total = total;
        this.current = 0;
    }

    public TestUtils(int percent, boolean outputEnable, boolean ouputMinimized, int totalToDo) {
        this(percent, outputEnable, ouputMinimized, System.currentTimeMillis(), totalToDo);
    }

    public TestUtils(boolean outputEnable, boolean ouputMinimized, int totalToDo) {
        this(-1, outputEnable, ouputMinimized, totalToDo);
    }

    public TestUtils(int totalToDo) {
        this(true, false, totalToDo);
    }

    public void setOutputEnable(boolean outputEnable) {
        this.outputEnable = outputEnable;
    }

    public void setOutputMinimized(boolean ouputMinimized) {
        this.ouputMinimized = ouputMinimized;
    }

    public void printProgress(int progression) {
        current += progression;
        if (!outputEnable || ouputMinimized) {
            return;
        }
        int lastpercent = (int) (current * (100d / total));
        if (percent == lastpercent) {
            return;
        }
        percent = lastpercent;
        long eta = current == 0 ? 0 : (total - current) * (System.currentTimeMillis() - startTime) / current;
        String etaHms = current == 0 ? "N/A" : String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(eta), TimeUnit.MILLISECONDS.toMinutes(eta) % TimeUnit.HOURS.toMinutes(1), TimeUnit.MILLISECONDS.toSeconds(eta) % TimeUnit.MINUTES.toSeconds(1));
        StringBuilder string = new StringBuilder(140);
//        @formatter:off
        string.append('\r')
        .append(String.join("", Collections.nCopies(percent == 0 ? 2 : 2 - (int) (Math.log10(percent)), " ")))
        .append(String.format(" %d%% [", percent))
        .append(String.join("", Collections.nCopies(percent, "=")))
        .append('>')
        .append(String.join("", Collections.nCopies(100 - percent, " ")))
        .append(']')
//        .append(String.join("", Collections.nCopies((int) (Math.log10(total)) - (int) (Math.log10(current)), " ")))
        .append(String.format(" %d/%d, ETA: %s", current, total, etaHms));
//      @formatter:on
        print(string);
        println();
    }

    public void print(Object... string) {
        if (outputEnable) {
            forcePrint(string);
        }
    }

    public void println(Object... string) {
        if (outputEnable) {
            forcePrintln(string);
        }
    }

    public void forcePrint(Object... string) {
        for (Object elem : string) {
            System.out.print(elem);
        }
    }

    public void forcePrintln(Object... string) {
        for (Object elem : string) {
            System.out.println(elem);
        }
    }
}
